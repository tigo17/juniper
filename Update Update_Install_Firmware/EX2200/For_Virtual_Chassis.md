## Install Junos OS on the EX switch via the Command Line Interface (CLI) prompt.

### For Virtual Chassis:
```sh
root@EX-2200:RE:0% cli
root@EX-2200> request system software add /var/tmp/jinstall-ex-10.0S10.1-domestic-signed.tgz member 1
```

Reboot the EX switch:
```sh
root@EX-2200> request system reboot all member
Reboot the system? [yes, no] (no) YES
```

After the reboot has completed, log in and verify that the new version of the software is properly installed:
```sh
root@EX-2200> show version
```

If you encounter a problem during the installation, go to KB20464 - [Upgrade EX switch] Stage 4 – Troubleshoot upgrade failure/crash – System stuck on Boot-up for troubleshooting upgrade failures.  

### Junos Installation Steps - Using a USB to copy the Junos OS software package to the EX switch

Format the USB Drive to FAT-32.

Copy the Junos OS software package that you downloaded from the Juniper support site in KB20313 - [Upgrade EX Switch] Stage 1 - Download Software to the USB.
(It is recommended to use a dedicated USB drive and copy only the Junos OS software package to it.)

On the EX switch, log in to shell mode:

```sh
{master:0}
root@EX-2200> start shell
root@EX-2200:RE:0%
```

Now perform the following steps:

Mount the USB:
```sh
root@EX-2200:RE:0% mount_msdosfs /dev/da1s1 /mnt
```

Copy the Junos OS software package from the USB to the /var/tmp directory of the EX switch:
```sh
root@EX-2200:RE:0% cd /mnt
root@EX-2200:RE:0% ls
root@EX-2200:RE:0% jinstall-ex-10.0S10.1-domestic-signed.tgz
root@EX-2200:RE:0% cp jinstall-ex-10.0S10.1-domestic-signed.tgz /var/tmp
```

Check whether the Junos OS software package is copied to the /var/tmp directory of the EX switch:
```sh
root@EX-2200:RE:0% cd /var/tmp
root@EX-2200:RE:0% ls
root@EX-2200:RE:0% jinstall-ex-10.0S10.1-domestic-signed.tgz
```

Note: If you need to verify the MD5 checksum of the Junos OS software package on the EX switch, refer to KB19931 - [Junos] Using the MD5 checksum to verify that the integrity of the Junos OS been pushed into the Juniper device.


Install Junos OS on the EX switch via the Command Line Interface (CLI) prompt:  

```sh
root@EX-2200:RE:0% cli
root@EX-2200> request system software add /var/tmp/jinstall-ex-10.0S10.1-domestic-signed.tgz member 1
```

Reboot the EX switch:

```sh
root@EX-2200> request system reboot all member
```

After the reboot has completed, log in and verify that the new version of the software is properly installed:

```sh
root@EX-2200> show version
```